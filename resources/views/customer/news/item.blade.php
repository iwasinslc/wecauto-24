@extends('layouts.auth')
@section('title', $new->content->title)

@section('content')


    <section class="page-preview">
        <div class="container">
            <div class="breadcrumbs"><a href="{{route('customer.main')}}">{{__('Home')}}</a><span>/</span><span>{{__('News')}}</span>
                    </div>
            <h1 class="page-preview__title"><span class="color-primary">{{$new->content->title}}</span>
            </h1>
        </div>
    </section>
    <section class="news-details js-animation" data-emergence="hidden">
        <div class="container">
            <div class="news-details__row">
                <div class="news-details__image"><img src="{{$new->content->getImgPath()}}" alt="">
                </div>
                <div class="news-details__content">
                    <p class="news-details__date">{{\Carbon\Carbon::parse($new->content->created_at)->format('d/m/Y')}}
                    </p>
                    <div class="news-details__block">
                        <p>{!! $new->content->teaser !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="news">
        <div class="container">
            <div class="typography">
                <div class="citation">
                    <p>{{$new->content->title}} </p>
                </div>
                {!! $new->content->text !!}
            </div>
        </div>
    </section>
{{--    <section class="news">--}}
{{--        <div class="container">--}}
{{--            <ul class="news-list" data-emergence="hidden">--}}
{{--                <li><a class="news-item" href="news-details.html"><span class="news-item__image"><img src="https://dummyimage.com/628x354/d2dcf0/fff" alt=""></span><span class="news-item__date">22/02/2020</span>--}}
{{--                        <p class="news-item__title">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod--}}
{{--                        </p>--}}
{{--                        <p class="news-item__desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do  eiusmod  tempor incididunt  ut labore et dolore magna aliqua.--}}
{{--                        </p><span class="news-item__arrow">--}}
{{--                      <svg class="svg-icon">--}}
{{--                        <use href="assets/icons/sprite.svg#icon-right-arrow"></use>--}}
{{--                      </svg></span></a>--}}
{{--                </li>--}}
{{--                <li><a class="news-item" href="news-details.html"><span class="news-item__image"><img src="https://dummyimage.com/628x354/d2dcf0/fff" alt=""></span><span class="news-item__date">22/02/2020</span>--}}
{{--                        <p class="news-item__title">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod--}}
{{--                        </p>--}}
{{--                        <p class="news-item__desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do  eiusmod  tempor incididunt  ut labore et dolore magna aliqua.--}}
{{--                        </p><span class="news-item__arrow">--}}
{{--                      <svg class="svg-icon">--}}
{{--                        <use href="assets/icons/sprite.svg#icon-right-arrow"></use>--}}
{{--                      </svg></span></a>--}}
{{--                </li>--}}
{{--                <li><a class="news-item" href="news-details.html"><span class="news-item__image"><img src="https://dummyimage.com/628x354/d2dcf0/fff" alt=""></span><span class="news-item__date">22/02/2020</span>--}}
{{--                        <p class="news-item__title">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod--}}
{{--                        </p>--}}
{{--                        <p class="news-item__desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do  eiusmod  tempor incididunt  ut labore et dolore magna aliqua.--}}
{{--                        </p><span class="news-item__arrow">--}}
{{--                      <svg class="svg-icon">--}}
{{--                        <use href="assets/icons/sprite.svg#icon-right-arrow"></use>--}}
{{--                      </svg></span></a>--}}
{{--                </li>--}}
{{--            </ul>--}}
{{--        </div>--}}
{{--    </section>--}}





@endsection