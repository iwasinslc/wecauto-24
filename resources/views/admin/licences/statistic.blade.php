@extends('admin/layouts.app')
@section('title')
    {{ __('Statistics') }}
@endsection
@section('breadcrumbs')
    <li> {{ __('Statistics') }}</li>
@endsection
@section('content')
    <!-- row -->


    <div class="row">
        <div class="col-md-12">
            <section class="tile tile-simple">
                <div class="tile-header dvd dvd-btm">
                    <h1 class="custom-font">
                        {{ __('Financial statistics by days') }}
                    </h1>
                    <strong style="float:right;">{{ __('Last') }} 30 {{ __('days') }}</strong>
                </div>
                <div class="tile-body">


                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>{{ __('Date') }}</th>
                                        @foreach ($licences as $licence)
                                            <th>{{ $licence->name }} {{ $licence->id }}</th>
                                        @endforeach

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach(array_reverse(getAdminLicensesTrafficStatistic(30)) as $day => $amounts)
                                        <tr>
                                            <td>{{ $day }}</td>
                                            @foreach ($amounts as $amount)
                                                <td style="font-weight: bold;">{{ $amount }}</td>
                                            @endforeach



                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>


                </div>
            </section>
        </div>
    </div>

@endsection
