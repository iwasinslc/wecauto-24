<section class="exchange-list">
    <div class="container">
        <div class="js-swiper-exchange swiper-container swiper-no-swiping">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="exc-item-small">
                        <p class="exc-item-small__title">WEC/USD
                        </p>
                        <div class="exc-item-small__bottom">
                            <p class="exc-item-small__count">{{rate('WEC', 'USD')}}
                            </p>
{{--                            <p class="exc-item-small__change exc-item-small__change--plus"><span>+12.1%</span>--}}
{{--                                <svg class="svg-icon">--}}
{{--                                    <use href="/assets/icons/sprite.svg#icon-arrow"></use>--}}
{{--                                </svg>--}}
{{--                            </p>--}}
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="exc-item-small">
                        <p class="exc-item-small__title">ACC/USD
                        </p>
                        <div class="exc-item-small__bottom">
                            <p class="exc-item-small__count">{{rate('ACC', 'USD')}}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="exc-item-small">
                        <p class="exc-item-small__title">GNT/USD
                        </p>
                        <div class="exc-item-small__bottom">
                            <p class="exc-item-small__count">{{rate('GNT', 'USD')}}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="exc-item-small">
                        <p class="exc-item-small__title">FST/USD
                        </p>
                        <div class="exc-item-small__bottom">
                            <p class="exc-item-small__count">{{rate('FST', 'USD')}}
                            </p>
                        </div>
                    </div>
                </div>
{{--                <div class="swiper-slide">--}}
{{--                    <div class="exc-item-small">--}}
{{--                        <p class="exc-item-small__title">BIP/USD--}}
{{--                        </p>--}}
{{--                        <div class="exc-item-small__bottom">--}}
{{--                            <p class="exc-item-small__count">{{rate('BIP', 'USD')}}--}}
{{--                            </p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
                <div class="swiper-slide">
                    <div class="exc-item-small">
                        <p class="exc-item-small__title">BTC/USD
                        </p>
                        <div class="exc-item-small__bottom">
                            <p class="exc-item-small__count">{{rate('BTC', 'USD')}}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="exc-item-small">
                        <p class="exc-item-small__title">ETH/USD
                        </p>
                        <div class="exc-item-small__bottom">
                            <p class="exc-item-small__count">{{rate('ETH', 'USD')}}
                            </p>
                        </div>
                    </div>
                </div>
{{--                <div class="swiper-slide">--}}
{{--                    <div class="exc-item-small">--}}
{{--                        <p class="exc-item-small__title">ETH/USD--}}
{{--                        </p>--}}
{{--                        <div class="exc-item-small__bottom">--}}
{{--                            <p class="exc-item-small__count">4,554.42--}}
{{--                            </p>--}}
{{--                            <p class="exc-item-small__change exc-item-small__change--plus"><span>-12.1%</span>--}}
{{--                                <svg class="svg-icon">--}}
{{--                                    <use href="/assets/icons/sprite.svg#icon-arrow"></use>--}}
{{--                                </svg>--}}
{{--                            </p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
        </div>
    </div>
</section>