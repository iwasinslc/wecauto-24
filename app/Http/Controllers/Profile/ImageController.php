<?php
namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

class ImageController extends Controller
{


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function imageCropPost(Request $request)
    {
        $data = $request->image;


        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);


        $data = base64_decode($data);
        $image_name= time().'.png';
        $path =  "avatars/" . $image_name;


        Storage::disk('s3')->put($path, $data, 'public');
        user()->update(['avatar'=>$path]);

        return response()->json(['success'=>'done']);
    }

    public function remove()
    {
        user()->update(['avatar'=>null]);
        return user()->getAvatarPath();
    }
}
