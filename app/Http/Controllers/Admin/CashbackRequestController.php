<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\RequestCashbackRequestUpdate;
use App\Models\CashbackRequest;
use Yajra\DataTables\DataTables;

class CashbackRequestController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('admin.cashback_requests.index');
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function dataTable()
    {
        $requests = CashbackRequest::query()
            ->with('user')
            ->get();

        return DataTables::of($requests)
            ->addIndexColumn()
            ->addColumn('show', function ($data) {
                return route('admin.cashback.show', ['cashback' => $data->id]);
            })
            ->addColumn('profile', function ($data) {
                return route('profile.cashback-result.show', ['id' => $data->id]);
            })
            ->addColumn('status', function($data) {
                return $data->approved === null
                    ? 'На рассмотрении'
                    : ($data->approved ? 'Одобрена' : 'Отклонена');
            })
            ->make(true);
    }

    /**
     * @param string $cashbackRequestId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function show(string $cashbackRequestId)
    {
        /** @var CashbackRequest $cashbackRequest */
        $cashbackRequest = CashbackRequest::query()
            ->with(['user', 'documents'])
            ->find($cashbackRequestId);

        if (!$cashbackRequest) {
            return redirect(route('admin.cashback.index'))
                ->with('error', __('Request not found'));
        }

        return view('admin.cashback_requests.show', [
            'cashbackRequest' => $cashbackRequest,
            'user' => $cashbackRequest->user,
            'documents' => $cashbackRequest->getMedia('cashback_request')
        ]);
    }

    /**
     * Update cashback request
     * @param RequestCashbackRequestUpdate $request
     * @param $cashbackRequestId
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Throwable
     */
    public function update(RequestCashbackRequestUpdate $request, $cashbackRequestId)
    {
        /** @var CashbackRequest $cashbackRequest */
        $cashbackRequest = CashbackRequest::findOrFail($cashbackRequestId);

        $update = [];
        if ($request->filled('documents_checked')) {
            $update['documents_checked'] = $request->documents_checked == 1;
        }
        if ($request->filled('video_checked')) {
            $update['video_checked'] = $request->video_checked == 1;
        }
        if ($request->filled('approved')) {
            if ($request->approved) {
                try {
                    // Approve cashback request
                    $cashbackRequest->update([
                        'approved' => true,
                        'result' => $request->result
                    ]);
                    return back()->with('success', __('Cashback request approved'));
                } catch (\Exception $e) {
                    return back()->with('error', $e->getMessage())->withInput();
                }
            } else {
                // Reject cashback request
                $update += [
                    'approved' => false,
                    'result' => $request->result
                ];
            }
        }

        if (!empty($update)) {
            $cashbackRequest->update($update);
            return back()->with('success', __('Cashback request updated'));
        } else {
            return back()->with('error', __('Updated data request is empty'));
        }
    }
}